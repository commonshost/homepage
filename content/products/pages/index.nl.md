+++
Title = "Pagina's"
Description = "Statische Website Hosting"
Slug = "paginas"
Summary = '''
Zeer snelle websites. Ondersteunt elke statische sitegenerator.

Serveer uw statische content via de Commons Host CDN voor snelheid, betrouwbaarheid en beveiliging.
'''
[[resources]]
name = "icon"
src = "gauge-full.svg"
title = "Icoon van een volle snelheidsmeter"
+++
