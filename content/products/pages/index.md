+++
Title = "Pages"
Description = "Very fast websites. All static site generators welcome."
Slug = "pages"
Summary = '''
Very fast websites. All static site generators welcome.

Serves your content through the Commons Host CDN for performance, reliability, and security.
'''
[[resources]]
name = "icon"
src = "gauge-full.svg"
title = "Icon showing full speedometer"
+++

## Usage

1. Install the latest version of [Node.js](https://nodejs.org).
1. Navigate a terminal window to the directory of your static website.
1. Run: `npx commonshost deploy`

{{< products/pages-deploy >}}

## Maximum Freedom of Speech

No censorship, except where required by law. Commons Host is a neutral platform, without content moderation by its operators.

CDN edge servers are hosted worldwide, under many jurisdictions with varying laws. Content that is allowed in some places may be banned in others. Commons Host follows the **Principle of Least Harm**: censoring only specifically banned content when accessed through servers physically located in that jurisdiction. This is intended to maximise two goals:

- *Hosting providers* subjected to local laws and regulations. E.g. copyright infringement, speech restrictions, or licensing requirements.

- *Authors and publishers* can publish whatever they are legally allowed to in their jurisdiction, without being limited by a lowest common denominator of global speech restrictions.

Commons Host CDN is a service designed to follow the law, not circumvent it. *Changing* the law is up to you.

## Security

Commons Host Pages makes web hosting secure by default.

- Static sites: Just inert files and directories. No server-side code is executed on every page load. This avoids the attack surfaces exposed by traditional Content Management Systems (CMS).

- Automatically encrypt your website traffic with a free [Let's Encrypt](https://letsencrypt.org) TLS certificate.

## Performance

Powered by the [Commons Host CDN]({{< relref "/products/cdn" >}}) edge servers for low latency access and fast web page load times.

- HTTP/2 support for all modern web browsers. For older browsers, or other HTTP clients, HTTP/1.1 is also supported.

- Brotli compression reduces the transfer size of all HTML, CSS, JSON, JavaScript, and other compressible files.

- [Server Push Manifests](https://help.commons.host/manifest/) to preload web page dependencies and avoid extra network round-trips.

## Bring Your Own Domain

Any custom domain name (e.g. `example.com`) is supported.

1. Configure your DNS provider with a `CNAME` record pointing to `commons.host`.
1. Then deploy your website using the `npx commonshost` command line tool.

If you do not have a custom domain name you can generate a free subdomain (`*.commons.host`). However, a subdomain is a dependency on Commons Host. Owning a custom domain name is important because it leaves you in control, should you ever wish to use another hosting provider. Property == Liberty
