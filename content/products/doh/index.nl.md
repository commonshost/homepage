+++
Title = "DNS over HTTPS"
Slug = "doh"
Layout = "doh"
Summary = '''
Privacy, veiligheid, snelheid. Geen Big Brother meer. Bescherm jezelf met Commons Host DNS over HTTPS om uw domeinnaamopzoekingen te coderen.
'''
[[resources]]
name = "icon"
src = "shield-lock.svg"
title = "Icoon van een privacy schild"
+++
