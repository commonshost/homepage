+++
Title = "Anywhere CDN"
Description = "United we serve. Divided we scale."
Slug = "cdn"
Summary = '''
Lots of tiny servers, everywhere. Protect and accelerate your content.

A network of physical edge servers owned and hosted by contributors, providing low-latency content to local users wherever needed.

Help grow the network. The Commons Host edge server software is lightweight and runs on as little as a Raspberry Pi or virtual server.
'''
[[resources]]
name = "icon"
src = "earth.svg"
title = "Icon showing the earth globe"
+++

The Commons Host CDN operates worldwide edge servers to deliver low-latency products and services like [Pages]({{< relref "/products/pages" >}}), [DoH]({{< relref "/products/doh" >}}), and [Gopher]({{< relref "/products/gopher" >}}).

## Network Map

{{< products/cdn-map >}}

## Low-Latency

The Commons Host CDN minimises latency by serving your content to users from a nearby edge server. Less distance between users and servers means a faster, better Internet experience.

Even at the speed of light, signals take time to travel long distances around the world. When several round trips are required to access a website, these delays add up to noticeable lag. Low-latency edge servers are a physical solution to a physical constraint.

## Geo DNS

The Commons Host CDN uses a technique called Geo DNS. Based on the geographic location of a user's IP address, a DNS lookup for `commons.host` will return an IP address of the most suitable CDN edge server.

Using Geo DNS offers precise control over routing. Other techniques such as Anycast IP addresses are at the mercy of [BGP](https://en.wikipedia.org/wiki/Border_Gateway_Protocol). Geo DNS allows edge servers to be located anywhere, not just at expensive and remote Internet Exchanges.

## Anti-DDoS

Safety in numbers: simple and effective. Any single edge server can be targetted and overwhelmed by network traffic, that is the reality of [DDoS attacks](https://en.wikipedia.org/wiki/Denial-of-service_attack). But the Commons Host CDN responds by routing legitimate user traffic to other edge servers. Like the [mythical Hydra](https://en.wikipedia.org/wiki/Lernaean_Hydra), as long as any edge servers are available, the CDN keeps serving users.

## Edge Proxy

<span class="coming-soon">Coming soon</span>

Route traffic to your own servers through the Commons Host CDN. Cache any HTTP content, like assets or API calls, at the network edge.

## Web Application Firewall

<span class="coming-soon">Coming soon</span>

Protect your servers, shielded by the Commons Host WAF. The Commons Host edge network detects and neutralises attacks based on the [OWASP ModSecurity Core Rule Set (CRS)](https://owasp.org/www-project-modsecurity-core-rule-set/).

## Decentralised Ownership

Volunteers own and contribute the physical infrastructure of Commons Host CDN. The network is currently managed by the Commons Host project administrators, but edge server owners can always choose other management. This leverage makes the overall CDN more resistant and resilient to censorship and other long-term threats inherent to centralised operators.

Anyone can deploy an edge server. This lets the network grow anywhere people live, not just on distant clouds.

## Commodity Hardware & Fibre

It ~~shouldn't~~ *doesn't* cost a fortune to run an edge server.

The Commons Host CDN runs on cost- and energy-efficient hardware like [Raspberry Pi](https://www.raspberrypi.org), [Odroid](https://www.hardkernel.com), other [Single Board Computers](https://www.explainingcomputers.com/sbc.html) (SBC), or [Virtual Private Servers](https://www.vpsbenchmarks.com/best_vps) (VPS).

The increasing availability of optical fibre in homes and offices makes self-hosting possible. Low latency and high bandwidth are no longer limited to specialised datacentres.

Large numbers of small servers is better than small numbers of large servers.

## Deploy Your Own PoP

Want to help? Run an edge server and join the Commons Host CDN! This makes Commons Host products & services lightning fast for yourself and other users near you.

You provide the infrastructure, then [send a patch](https://gitlab.com/commonshost/ansible/-/edit/master/hosts/edge.production.yaml) to add your server to the inventory.

Feel free to get in touch with [@commonshost](https://twitter.com/commonshost) on Twitter, or [Open an Issue](https://gitlab.com/commonshost/support/-/issues/new) on GitLab.

Some factors to consider:

- Location: Anywhere. The closer to more people, the better.
- Uptime: Best effort. Don't worry, monitoring automatically detects unavailable servers and routes traffic elsewhere.
- Hardware: Run what you have. ARM and Intel/AMD are supported. Use an SSD for storage endurance.
- Software: Dedicated machines must run Ubuntu, Raspberry Pi OS (Raspbian), or some other Debian-based Linux. Docker support is planned.
- Ports: `70` (Gopher), `80` (HTTP), `443` (HTTPS), `2222` (SSH, re-mapped from `22` to avoid port scanners).
- Security: Physical access must be restricted to Nice People<sup>™️</sup>. That's hopefully yourself and trusted collaborators. WIP: deployment in untrusted and adversarial environments for origin-restricted CDN tiers and cryptographically signed content (e.g. [DNSSEC](https://www.icann.org/resources/pages/dnssec-what-is-it-why-important-2019-03-05-en), [SXG](https://github.com/WICG/webpackage)).
- Network: Public IPv4 address. Dynamic or static, both are supported. IPv6 support is planned.
- Bandwidth: Very little bandwidth is required for the currently offered services (static websites, Gopher, DoH). Bandwidth budgets are planned.
- Latency: Fibre is preferred since it offers single-digit or sub-millisecond latency. Cable or DSL can work, if proximity to users compensates for the connection latency (~1ms/100km).
