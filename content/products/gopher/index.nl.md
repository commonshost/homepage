+++
Title = "Gopher"
Description = "Surf het Net alsof het 1991 is!"
Slug = "gopher"
Summary = '''
Herontdek het oorspronkelijke Internet van het op tekst gebaseerde Gopher-protocol.

Bouw en host een Gopherhole op Commons Host. Surf door het Gopherverse via de Commons Host Gopher over HTTP proxydienst.
'''
[[resources]]
name = "icon"
src = "noun_Rodent_1415987.svg"
title = "Icoon van een goffer knaagdier"
+++
