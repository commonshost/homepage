+++
Title = "Gopher"
Description = "Surf the Net like it's 1991!"
Slug = "gopher"
Summary = '''
Rediscover the early-days Internet of the text based Gopher protocol.

Build and host a Gopherhole on Commons Host. Browse the Gopherverse via the Commons Host Gopher over HTTP proxy service.
'''
[[resources]]
name = "icon"
src = "noun_Rodent_1415987.svg"
title = "Icon showing a gopher animal"
+++

## Browser

[Commons Host Gaufre](https://gopher.commons.host) is a browser for the [Gopher protocol](https://en.wikipedia.org/wiki/Gopher_%28protocol%29). It runs in your web browser, safely sandboxed, with no need to install a native app or grant any permissions. Gaufre uses a proxy tunnel to access both plaintext and encrypted Gopher servers. Gaufre supports Gopher menus and forms, text, images, video, audio, PDF, binary file downloads, and sandboxed HTML.

Gaufre: https://gopher.commons.host

## Hosting & CDN

Publish your own Gopherhole on Commons Host Pages with support for:

- Encryption using [Gopher over TLS (GoT)](https://gitlab.com/commonshost/goth)
- Gopher Content Delivery Network (CDN)
- Dual-stack HTTP and Gopher hosting on the same domain

Instructions:

1. Create a `_gopher` subdirectory in your static site. This is the Gopherhole root path.

    ```shell
    cd ~/website # ... use your static site path
    mkdir _gopher
    ```

1. Statically generate your Gophermaps and other content within `_gopher`

    ```shell
    cd _gopher
    echo "iHello World\r\n.\r\n" > ./gophermap
    ```

1. Deploy your site to [Commons Host Pages]()

    ```shell
    npx commonshost deploy
    ```

## Proxy

The Commons Host hosting CDN operates a public [Gopher over HTTP (GoH)](https://gitlab.com/commonshost/goh) service. This service is not currently monitored, restricted, nor rate-limited. Please use this responsibly, with respect for the Gopher community.

For the best performance and privacy, run your own GoH proxy locally.

```shell
$ npx goh
Listening on http://localhost:7080
GET /url=gopher://gopher.floodgap.com
```

```shell
$ curl -H 'Accept: application/gopher' \
       'http://localhost:7080?url=gopher://gopher.floodgap.com'
iWelcome to Floodgap Systems' official gopher server.		error.host	1
iFloodgap has served the gopher community since 1999		error.host	1
...
```
