# / # homepage

Internet Freedom of/by/for the People.

- DNS Resolver
- Static Hosting
- Global CDN

Created equal.

Every product offered by Commons Host is developed as 100% open source under permissive licenses.
This gives everyone the freedom to inspect, modify, redistribute, and self-host.

When service providers fear users, there is liberty.
When users fear service providers, there is tyranny.

Absolute openness is a fundamental check-and-balance against operators of Internet services.
If trust in an operator is compromised, users can migrate to another operator of the same stack.
Likewise, an operator who fears losing customers is less likely to abuse their trust.

Providers may charge fees, or refuse to provide service to whomever they do not want as their customer.

Concentrated ownership threatens the Internet's independence.
Internet services increasingly operated by the world's largest technology companies.

Commons Host empowers individuals to self-host edge servers, forming a global network together.
with infrastructure owned by the people, . users and publishers we are stronger than any cloud company.

Decisions ultimately are made by owners of infrastructure.

Commons Host offers free and open source Internet infrastructure.

Resistance to censorship through total transparency, freedom of association, and distributed ownership of physical network infrastructure.

Commons Host offers fundamental Internet services like web hosting, DNS resolving, and a global CDN.

ownership of the physical is retained by collaborators and users.

## /blog
- (port over all the dev.to posts)
- ...
## /shop
- server
- tshirt
- stickers
## /pricing
- DNS requests
- HTTP requests
- Bandwidth
- Storage
## /about
- mission
- collaborators
  ### /about/privacy-policy
  ### /about/terms-of-service
  ### /about/accetable-use-policy
## /products
- 100% FOSS
- Free from lock-in
  ### /products/cdn
  - map of edge servers
  - proxy for your own backend HTTP server
  - automatically used by Commons Host Sites
  - extend the network by connecting your own server
  ### /products/dns
  - filters
  - local resolver
  - DoH provider benchmark
  - local server on private hardware for guaranteed privacy and control
  ### /products/pages
  - static web & gopher hosting
  ### /products/gopher
  - gopherhole hosting
  - GoH proxy
  ### /products/future
  - Monitoring
  - Compute
  - DNS hosting
  - WebRTC
  - IPFS
  - Messaging/Queues
  - Database
## /solutions
  ### /solutions/dns-privacy-security
  - Education, family, business
  ### /solutions/website-performance
  - Global network
  ### /solutions/cdn-edge-anywhere
  - Extend the network anywhere you want with your own hardware
## /help
  ### /help/status
  - network status
  - api
  - uptime
  ### /help/contact
  - Contact via GitLab/GitHub issues
  - Contact via Twitter
  - Contact via Email
  ### /help/guides
  - Creating an account
  - Static web site hosting
  - Deploying from CI/CD
  - Gopher hole publishing
  - Using DNS over HTTPS
  - Server Push Manifests
  - URL redirects
  - Custom headers
  - Fallback page for client-side routing
  - ...
  ### /help/presentations
  - Youtube playlist/videos about Commons Host
  ### /help/documentation
  - List of projects
  - Each project contains its own docs
    #### /help/documentation/cli
    #### /help/documentation/dohnut
    #### /help/documentation/bulldohzer
    #### /help/documentation/server
    #### /help/documentation/manifest
    #### /help/documentation/core
    #### /help/documentation/unbundle

Services:
- Static Hosting: Web, Gopher
- DNS over HTTPS Resolver:
  - Public resolver
    - Free of charge
    - Freedom of speech: No censorship
    - Private: No logging, no ISP middleman
  - Private resolver
    - Filtering: Ad-blocking, family friendly content, security/malware, custom block lists
    - Monitoring: Dashboard with DNS stats
    - On-premise + cloud hybrid: Self-hosted, local hardware on LAN, fallback to cloud service
    - Fallback

- Infrastructure Marketplace
- Web
  - Static Web Hosting
- DNS
  - Resolver
    - Public
    - Private
  - Records: Hosting & Management
  - Domain Name Registration
- Compute

## Attribution

### `earth.svg`

- Author: Google
- License: MIT
- Reference: https://materialdesignicons.com/icon/earth

### `gauge-full.svg`

- Author: GreenTurtwig
- License: MIT
- Reference: https://materialdesignicons.com/icon/gauge-full

### `noun_Rodent_1415987.svg`

- Author: Jack Zwanenburg, NL
- License: CC-BY
- Reference: https://thenounproject.com/search/?i=1415987

### `shield-lock.svg`

- Author: GreenTurtwig
- License: MIT
- Reference: https://materialdesignicons.com/icon/shield-lock

### `noun_hierarchy_1314283.svg`

- Author: Badsha Mia
- License: CC-BY
- Reference: https://thenounproject.com/search/?i=1314283

### `noun_end user_1677704.svg`

- Author: Becris
- License: CC-BY
- Reference: https://thenounproject.com/search/?i=1677704
